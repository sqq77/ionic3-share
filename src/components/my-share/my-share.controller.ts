import { Injectable } from '@angular/core';
import { App } from "ionic-angular";
import { MyShare } from './my-share';
import { MyShareOptions } from "./my-share.options";

@Injectable()
export class MyShareController {

  constructor(private _app: App) {
  }

  create(opts: MyShareOptions = {}):MyShare {
    return new MyShare(this._app,opts);
  };

}