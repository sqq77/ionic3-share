
import { App, NavOptions, ViewController } from "ionic-angular";
import { MyShareComponent } from './my-share.component';
import { MyShareOptions } from "./my-share.options";


export class MyShare extends ViewController {
  private _app: App;

  constructor(app: App, opts: MyShareOptions) {
    super(MyShareComponent, opts, null);
    this._app = app;
  }

  present(navOptions: NavOptions = {}): Promise<any> {
    let opts = {
      direction: 'Enter'
    }
    return this._app.present(this, opts);
  }

  getTransitionName(direction: string): string {
    const key = 'actionSheet' + (direction === 'back' ? 'Leave' : 'Enter');
    return this._nav && this._nav.config.get(key);
  }
}
