import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MyShareController } from '../../components/my-share/my-share.controller';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  event: any;

  constructor(public navCtrl: NavController, public myShareCtrl: MyShareController) {
    
    
    //假数据
    this.event = {
			attachs: [
				{url: 'http://drip.growu.me/img/icon.png'}
			],
			user: {
				nickname: '李彦宏'
			},
			content:'测试数据测试数据测试数据测试数据测试数据'
		}
  }

  //分享
	doShare(){
		let image = null;
		if (this.event.attachs.length > 0) {
			let attach = this.event.attachs[0];
			if (attach.url) {
				image = attach.url;
			}
		}
		let myShare = this.myShareCtrl.create({
			data: {
					type: 'url',
					title: this.event.user.nickname + " 的动态",
					description: this.event.content,
					image: image,
					thumb: image,
					url: "http://drip.growu.me",
			},
			extra:this.event
		});
		myShare.present();
	}

}
